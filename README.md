![Alt text](https://raw.githubusercontent.com/JordanMicahBennett/BRAIN-UNIVERSE-SYNONYMOUS-INTERFACE/master/source-code/data/images/captures/5.png "default page")
=============================







![Alt text](https://raw.githubusercontent.com/JordanMicahBennett/BRAIN-UNIVERSE-SYNONYMOUS-INTERFACE/master/source-code/data/images/captures/0.png "default page")
=============================
![Alt text](https://raw.githubusercontent.com/JordanMicahBennett/BRAIN-UNIVERSE-SYNONYMOUS-INTERFACE/master/source-code/data/images/captures/1.png "default page")
=============================
![Alt text](https://raw.githubusercontent.com/JordanMicahBennett/BRAIN-UNIVERSE-SYNONYMOUS-INTERFACE/master/source-code/data/images/captures/2.png "default page")
=============================
![Alt text](https://raw.githubusercontent.com/JordanMicahBennett/BRAIN-UNIVERSE-SYNONYMOUS-INTERFACE/master/source-code/data/images/captures/3.png "default page")
=============================





BRAIN UNIVERSE SYNONYMOUS INTERFACE
===================================

An operating system interface modelled on the cosmos.


ALTERNATIVE NAMES
===================================
  i.illumium
  ii.unicortex


VIDEO(S)
===================================
https://www.youtube.com/watch?v=p2Apn-gTzqU



WEBSITE
===================================
http://illumium.yzi.me/




AUTHOR PORTFOLIO
============================================
http://folioverse.appspot.com/
